package com.example.wiremockTest;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class WiremockTestApplicationTests {

	private static WireMockServer wireMockServer;

	@BeforeAll
	public void setup(){
		wireMockServer = new WireMockServer(4567);
		wireMockServer.start();

	}

	@Test
	public void wireMockTest() throws InterruptedException {
		testInitPostMock("/test", "123");
		testInitPostMockWithRequestBody("/test", "key=value1", "111");
		testInitPostMockWithRequestBody("/test", "key=value2", "222");
		Thread.sleep(30000);
	}

	@Test
	void contextLoads() {
	}

	@AfterAll
	public static void teardown() {
		wireMockServer.stop();
	}

	public static void testInitPostMock(String  endpointUrl, String response){
		wireMockServer.stubFor(post(urlEqualTo(endpointUrl))
				.willReturn(aResponse().withBody(response)));
	}

	public static void testInitPostMockWithRequestBody(String  endpointUrl,String requestBody, String response){
		wireMockServer.stubFor(post(urlEqualTo(endpointUrl)).withRequestBody(equalTo(requestBody))
				.willReturn(aResponse().withBody(response)));
	}
}
